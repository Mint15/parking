import java.util.ArrayList;

public class Parking {
    private final ArrayList<Car> carsPlaces;
    private final ArrayList<Car> trucksPlaces;
    private int freePlaces;
    private int movesLeft;
    private final int capacity;
    private int freePlacesTruck;
    private int movesLeftTruck;
    private final int capacityTruck;
    private int nearestPlace;
    private int nearestPlaceTruck;

    public Parking(int spacesForCars, int spacesForTrucks) {
        this.freePlaces = spacesForCars;
        this.movesLeft = 0;
        this.capacity = spacesForCars;
        this.nearestPlace = 0;
        this.carsPlaces = new ArrayList<>(spacesForCars);
        carsPlaces.ensureCapacity(spacesForCars);
        this.freePlacesTruck = spacesForTrucks;
        this.movesLeftTruck = 0;
        this.capacityTruck = spacesForTrucks;
        this.nearestPlaceTruck = 0;
        this.trucksPlaces = new ArrayList<>(spacesForTrucks);
        trucksPlaces.ensureCapacity(spacesForTrucks);
    }

    public int returnFreePlaces(String type) {
        if (type.equals("trucks")) {
            for (int i = 0; i < trucksPlaces.size(); i++) {
                if (trucksPlaces.get(i) == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < carsPlaces.size(); i++) {
                if (carsPlaces.get(i) == null) {
                    return i;
                }
            }
        }
        return 0;
    }

    public void parkCar(Car car, int moves) {
        int n = returnFreePlaces("car");
        car.setMoves(moves);
        carsPlaces.add(n, car);
        carsPlaces.set(n, car).setPlace(n + 1);
        setFreePlaces(getFreePlaces() - 1);
        car.setParkingType("парковке для легковых машин");
        System.out.println("Легковая машина с id " + Manager.id + " припаркована на месте номер " + car.getPlace() +
                " на " + car.getMoves() + " хода");
    }

    public void parkTruck(Car car, int moves) {
        int n = returnFreePlaces("trucks");
        car.setMoves(moves);
        trucksPlaces.add(n, car);
        trucksPlaces.set(n, car).setPlace(n + 1);
        setFreePlacesTruck(getFreePlacesTruck() - 1);
        car.setParkingType("парковке для грузовых машин");
        System.out.println("Грузовая машина с id " + Manager.id + " припаркована на месте номер " + car.getPlace() +
                " на " + car.getMoves() + " хода");
    }

    public void parkDouble(Car car, int moves) {
        int n = returnFreeDoublePlaces();
        for (int i = n; i < (n + 2); i++) {
            car.setMoves(moves);
            carsPlaces.add(i, car);
            carsPlaces.set(i, car).setPlace(i + 1);
            setFreePlaces(getFreePlaces() - 1);
            car.setParkingType("парковке для легковых машин");
        }
        System.out.println("Грузовая машина с id " + Manager.id + " припаркована на места для легковых автомобилей " +
                "номер " + (car.getPlace() - 1) + " и номер " + car.getPlace() +
                " на " + car.getMoves() + " хода");
    }

    public void countMovesLeft() {
        countMovesLeft("car");
        countMovesLeft("trucks");
    }

    public void countMovesLeft(String type) {
        int min = Manager.total + 1;
        if (type.equals("car")) {
            for (int j = 0; j < carsPlaces.size(); j++) {
                if (carsPlaces.get(j) != null) {
                    if (carsPlaces.get(j).getMoves() < min) {
                        min = carsPlaces.get(j).getMoves();
                        setNearestPlace(j + 1);
                    }
                }
            }
            setMovesLeft(min);
        } else {
            for (int j = 0; j < trucksPlaces.size(); j++) {
                if (trucksPlaces.get(j) != null) {
                    if (trucksPlaces.get(j).getMoves() < min) {
                        min = trucksPlaces.get(j).getMoves();
                        setNearestPlaceTruck(j + 1);
                    }
                }
            }
            setMovesLeftTruck(min);
        }
    }

    public void clearParking() {
        for (int l = 0; l < carsPlaces.size(); l++) {
            carsPlaces.set(l, null);
            setMovesLeft(0);
        }
        for (int l = 0; l < trucksPlaces.size(); l++) {
            trucksPlaces.set(l, null);
            setMovesLeftTruck(0);
        }
        setFreePlaces(getCapacity());
        setFreePlacesTruck(getCapacityTruck());
    }

    public void makeTurn() {
        for (int j = 0; j < trucksPlaces.size(); j++) {
            if (trucksPlaces.get(j) != null) {
                int moves = trucksPlaces.get(j).getMoves();
                if (moves == 0) {
                    trucksPlaces.set(j, null);
                    setFreePlacesTruck(getFreePlacesTruck() + 1);
                } else {
                    trucksPlaces.get(j).setMoves(moves - 1);
                }
            }
        }
        for (int i = 0; i < carsPlaces.size(); i++) {
            if (carsPlaces.get(i) != null) {
                int moves = carsPlaces.get(i).getMoves();
                if (moves == 0) {
                    carsPlaces.set(i, null);
                    setFreePlaces(getFreePlaces() + 1);
                } else {
                    carsPlaces.get(i).setMoves(moves - 1);
                }
            }
        }
        countMovesLeft();
    }

    public void showCars() {
        int size = carsPlaces.size();
        for (int i = 0; i < size; i++) {
            if (carsPlaces.get(i) != null) {
                if (i < size - 1 && carsPlaces.get(i + 1) != null &&
                        carsPlaces.get(i).id == carsPlaces.get(i + 1).id) {
                    System.out.println("Грузовик с номером " + carsPlaces.get(i).id + " будет стоять" +
                            " на местах номер " + (i + 1) + " и " + (i + 2) + " на " +
                            carsPlaces.get(i).getParkingType() + " еще " + carsPlaces.get(i).moves + " ходов");
                    i++;
                } else {
                    System.out.println("Легковой автомобиль с номером " + carsPlaces.get(i).id + " будет стоять" +
                            " на месте номер " + (i + 1) + " на " + carsPlaces.get(i).getParkingType() +
                            " еще " + carsPlaces.get(i).moves + " ходов");
                }
            }
        }
        for (int i = 0; i < trucksPlaces.size(); i++) {
            if (trucksPlaces.get(i) != null) {
                System.out.println("Грузовик под номером " + trucksPlaces.get(i).id + " будет стоять на месте номер "
                        + (i + 1) + " на " + trucksPlaces.get(i).getParkingType() +
                        " еще " + trucksPlaces.get(i).moves + " ходов");
            }
        }
    }

    public int returnFreeDoublePlaces() {
        for (int i = 0; i < (carsPlaces.size() - 1); i++) {
            if (carsPlaces.get(i) == null) {
                if (trucksPlaces.get(i + 1) == null) {
                    return i;
                }
            }
        }
        return 0;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(int freePlaces) {
        this.freePlaces = freePlaces;
    }

    public int getMovesLeft() {
        return movesLeft;
    }

    public void setMovesLeft(int movesLeft) {
        this.movesLeft = movesLeft;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getNearestPlace() {
        return nearestPlace;
    }

    public void setNearestPlace(int nearestPlace) {
        this.nearestPlace = nearestPlace;
    }

    public int getFreePlacesTruck() {
        return freePlacesTruck;
    }

    public void setFreePlacesTruck(int freePlacesTruck) {
        this.freePlacesTruck = freePlacesTruck;
    }

    public int getMovesLeftTruck() {
        return movesLeftTruck;
    }

    public void setMovesLeftTruck(int movesLeftTruck) {
        this.movesLeftTruck = movesLeftTruck;
    }

    public int getCapacityTruck() {
        return capacityTruck;
    }

    public int getNearestPlaceTruck() {
        return nearestPlaceTruck;
    }

    public void setNearestPlaceTruck(int nearestPlaceTruck) {
        this.nearestPlaceTruck = nearestPlaceTruck;
    }

}