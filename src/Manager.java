import java.util.Random;
import java.util.Scanner;

public class Manager {
    public static boolean isWork = true;
    static int id = 1000;
    static Random random = new Random();
    static Parking parking;
    static public int total;

    public static void main(String[] agr) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество парковочных мест для легковых автомобилей:");
        int spacesForCars = scanner.nextInt();
        System.out.println("Введите количество парковочных мест для грузовых автомобилей:");
        int spacesForTrucks = scanner.nextInt();
        parking = new Parking(spacesForCars, spacesForTrucks);

        total = ((spacesForCars + spacesForTrucks) / 3) + 1;

        do {
            help();
        } while (isWork);
        System.out.println("\n_________Парковка закрыта_________\n");
    }

    public static void help() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nВведите номер дальнейшей команды:\n");
        System.out.println("1 - завершить ход и перейти к следующему\n" + "2 - проверить сколько мест занято, " +
                "сколько свободно, когда освободится ближайшее\n" + "3 - показать список стоящих на парковке" +
                " машин\n4 - очистить парковку от всех машин\n5 - закрыть парковку\n");
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                parking.makeTurn();
                int number1 = random.nextInt(total);
                if (number1 != 0) {
                    System.out.println("\nНа парковку въехало " + number1 + " легковых(ая/ые) машин(а/ы):");
                    do {
                        carMove();
                        number1--;
                    } while (number1 > 0);

                } else {
                    System.out.println("\nЗа этот ход не приехало ни одной легковой машины");
                }
                int number2 = random.nextInt(total);
                if (number2 != 0) {
                    System.out.println("\nНа парковку въехало " + number2 + " грузовых(ая/ые) машин(а/ы):");
                    do {
                        truckMove();
                        number2--;
                    } while (number2 > 0);
                } else {
                    System.out.println("\nНа парковку не въехало ни одной грузовой машины");
                }

                break;
            case "2":
                parking.countMovesLeft("car");
                parking.countMovesLeft("truck");
                check();
                break;
            case "4":
                parking.clearParking();
                System.out.println("\nПарковка очищена! Хозяева машин этому не особо рады...");
                break;
            case "5":
                isWork = false;
                break;
            case "3":
                parking.showCars();
            default:
                help();
                break;
        }
    }

    private static void truckMove() {
        Car car = new Car(id, "truck");
        if (parking.getFreePlacesTruck() > 0) {
            int moves = (random.nextInt(10) + 1);
            parking.parkTruck(car, moves);
        } else {
            if (parking.getFreePlaces() >= 2) {
                int moves = (random.nextInt(10) + 1);
                parking.parkDouble(car, moves);
            } else {
                System.out.println("Простите, все места заняты");
            }
        }

        id++;
    }

    private static void check() {
        System.out.println("\nВсего мест на парковке для легковых автомобилей: " + parking.getCapacity());
        System.out.println("Занято: " + (parking.getCapacity() - parking.getFreePlaces()));
        System.out.println("Свободно: " + parking.getFreePlaces() + "\n");
        if (parking.getFreePlaces() == 0) {
            System.out.println("Место номер " + parking.getNearestPlace() + " освободится через "
                    + parking.getMovesLeft() + " ходов \n");
        }
        System.out.println("Всего мест на парковке для грузовых автомобилей: " + parking.getCapacityTruck());
        System.out.println("Занято: " + (parking.getCapacityTruck() - parking.getFreePlacesTruck()));
        System.out.println("Свободно: " + parking.getFreePlacesTruck() + "\n");
        if (parking.getFreePlacesTruck() == 0) {
            System.out.println("Место номер " + parking.getNearestPlaceTruck() +
                    " освободится через " + parking.getMovesLeftTruck() + " ходов \n");
        }
    }

    private static void carMove() {
        Car car = new Car(id, "car");
        if (parking.getFreePlaces() > 0) {
            int moves = (random.nextInt(10) + 1);
            parking.parkCar(car, moves);
        } else {
            System.out.println("Простите, все места заняты");
        }
        id++;
    }
}