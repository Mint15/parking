public class Car {
    int id;
    int place;
    int moves;
    String parkingType;
    String type;

    public Car(int id, String type) {
        this.id = id;
        this.place = 0;
        this.moves = 0;
        this.parkingType = ".";
        this.type = type;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getMoves() {
        return moves;
    }

    public void setMoves(int moves) {
        this.moves = moves;
    }
}
